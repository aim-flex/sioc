#define __STRICT_ANSI__

#include <thread>

#include "json.hpp"
#include "fifo_map.hpp"

// add disconnect : enet_peer_disconnect_now

template<class K, class V, class dummy_compare, class A>
using my_workaround_fifo_map = nlohmann::fifo_map<K, V, nlohmann::fifo_map_compare<K>, A>;

using json = nlohmann::basic_json<my_workaround_fifo_map>;

#include "enet/enet.h"

#include "enetpp/client.h"
#include "enetpp/server.h"

#include "base64.h"

#include <cpp_redis/cpp_redis>
#include <tacopie/tacopie>

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <signal.h>
#include <fstream>

#ifdef _WIN32
#include <Winsock2.h>
#endif /* _WIN32 */

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib.h"

#undef min
#undef max

#include "jwt/jwt.hpp"

std::string ec_private_key = "\
-----BEGIN EC PRIVATE KEY-----\r\n\
MHcCAQEEIEskVXN5S6pWqIxTxPAEKXF44twtAjKeFWmalcHU4FlqoAoGCCqGSM49\r\n\
AwEHoUQDQgAEF0Gp9S8+FGQBz8w1Qzpcscev1A/y4cwkZ3dPzLIPoQx7TpJeCOK4\r\n\
2FrqNpmSCjo77mii1VArK0Qoh6gye8V7pw==\r\n\
-----END EC PRIVATE KEY-----\r\n";

std::string ec_public_key = "\
-----BEGIN PUBLIC KEY-----\r\n\
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEF0Gp9S8+FGQBz8w1Qzpcscev1A/y\r\n\
4cwkZ3dPzLIPoQx7TpJeCOK42FrqNpmSCjo77mii1VArK0Qoh6gye8V7pw==\r\n\
-----END PUBLIC KEY-----\r\n";

enum ChannelType
{
	channel_invalid = -1,
	channel_all = 0,
	channel_loader = 1,
	channel_cheat = 2
};

class Client
{
public:
	unsigned int id = 0;
	bool authenticated = false;
	std::string name = "";
	ChannelType channel = channel_invalid;
	int cheat_id = 0;
public:
	Client() : id(0) {}

	// yikes from me dawg, warnuing not camelcase cuz header!!!
	unsigned int get_id() const
	{
		return id;
	}
	void set_id(unsigned int uid)
	{
		this->id = uid;
	}
};

struct NetworkCallback
{
	NetworkCallback(std::string name, ChannelType channel, std::function<void(Client&, json&)> callback) : name(name), channel(channel), callback(callback) {}

	std::string name;
	ChannelType channel;
	std::function<void(Client&, json&)> callback;
};

std::vector<uint8_t> read_file(const std::string& file_path)
{
	std::ifstream stream(file_path, std::ios::binary);

	stream.unsetf(std::ios::skipws);

	auto buffer = std::vector<uint8_t>();
	stream.seekg(0, std::ios::end);
	buffer.reserve(stream.tellg());
	stream.seekg(0, std::ios::beg);

	std::copy(std::istream_iterator<uint8_t>{stream}, {}, std::back_inserter(buffer));

	return buffer;
}

auto empty = Client();

class Server
{
private:
	std::mutex mutex;
	bool stop = false;
	std::deque<NetworkCallback> callbacks;
public:
	enetpp::server<Client> server;
	enetpp::trace_handler trace_handler;

	Server()
	{
		enetpp::global_state::get().initialize();

		trace_handler = [&](const std::string& msg)
		{
			std::lock_guard<std::mutex> lock(mutex);

			printf("%s\n", msg.c_str());
		};

		server.set_trace_handler(trace_handler);
	}

	void Start()
	{
		static unsigned int next_uid = 0;

		auto init_client_func = [&](Client& client, const char* ip)
		{
			next_uid++;

			client.set_id(next_uid);
		};

		server.start_listening(enetpp::server_listen_params<Client>()
			.set_max_client_count(512)
			.set_channel_count(1)
			.set_listen_port(3030)
			.set_initialize_client_function(init_client_func));

		Listen();
	}
	void Stop()
	{
		stop = true;
	}
	void On(const std::string& name, ChannelType channel, std::function<void(Client&, json&)> function)
	{
		callbacks.emplace_back(name, channel, function);
	}

	Client* FindAuthenticatedClientByName(ChannelType channel, const std::string& name)
	{
		for (auto& client : server.get_connected_clients())
		{
			if (client->channel == channel && client->name == name && client->authenticated)
				return client;
		}

		return &empty;
	}

	Client* FindClientByName(ChannelType channel, const std::string& name)
	{
		for (auto& client : server.get_connected_clients())
		{
			if (client->channel == channel && client->name == name)
				return client;
		}

		return &empty;
	}

	const std::vector<Client*>& GetConnectedClients()
	{
		return server.get_connected_clients();
	}

	void Send(const Client& client, json json)
	{
		auto cbor = json::to_cbor(json);

		server.send_packet_to(client.id, 0, cbor.data(), cbor.size(), ENET_PACKET_FLAG_RELIABLE);
	}
	void Listen()
	{
		while (server.is_listening())
		{
			auto on_client_connected = [&](Client& client)
			{
				try
				{
					json empty;

					for (auto& callback : callbacks)
					{
						if (callback.name == "on_client_connected" && (callback.channel == client.channel || callback.channel == channel_all))
							callback.callback(client, empty);
					}
				}
				catch (...)
				{

				}

				trace_handler("on_client_connected");
			};

			auto on_client_disconnected = [&](unsigned int client_uid)
			{
				try
				{
					json empty;

					Client temp;
					temp.set_id(client_uid);

					for (auto& callback : callbacks)
					{
						if (callback.name == "on_client_disconnected")
							callback.callback(temp, empty);
					}
				}
				catch (...)
				{

				}

				trace_handler("on_client_disconnected");
			};

			auto on_client_data_received = [&](Client& client, const enet_uint8* data, size_t data_size)
			{
				try
				{
					json j_from_cbor = json::from_cbor(data, data_size);

					std::string callback_name = j_from_cbor[0];
					json message = j_from_cbor[1];

					for (auto& callback : callbacks)
					{
						if (callback.name == callback_name && (callback.channel == client.channel || callback.channel == channel_all))
							callback.callback(client, message);
					}
				}
				catch (std::exception& e)
				{
					trace_handler(e.what());

					//server.disconnect_client_now(&client);
				}
			};

			server.consume_events(on_client_connected, on_client_disconnected, on_client_data_received);

			if (stop)
			{
				server.stop_listening();
			}

			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
	}

	~Server()
	{
		enetpp::global_state::get().deinitialize();
	}
};

Server server;

struct PubQueue
{
	PubQueue(const char* event, json data) : event(event), data(data) {}

	std::string event;
	json data;
};

int main()
{
	using namespace jwt::params;

	server.trace_handler("server started\n");

	cpp_redis::client client;
	std::deque<PubQueue> publish_queue;

	std::thread thread_redis_client([&]
	{
		client.connect("localhost", 6379);

		server.trace_handler("connected redis client (pub)\n");

		server.trace_handler("published to redis (pub)");

		while (1)
		{
			for (auto& queue : publish_queue)
			{
				server.trace_handler(queue.data.dump());

				client.publish(queue.event, queue.data.dump());
			}

			client.commit();
			publish_queue.clear();

			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	});

#ifdef _WIN32
	//! Windows netword DLL init
	WORD version = MAKEWORD(2, 2);
	WSADATA data;

	if (WSAStartup(version, &data) != 0) {
		std::cerr << "WSAStartup() failure" << std::endl;

		while (1)
			std::this_thread::sleep_for(std::chrono::milliseconds(100));

		//return -1;
	}
	else
		server.trace_handler("WSAStartup successful\n");

#endif /* _WIN32 */

	server.On("login", channel_all, [&](Client& client, json& message)
	{
		try
		{
			ChannelType channel = message[0];
			server.trace_handler(std::to_string(channel));

			if (channel == channel_loader)
			{
				std::string name = message[1];
				std::string password = message[2];
				std::string hwid = message[3];

				httplib::Client cli("localhost", 80);

				httplib::Params params;
				params.emplace("username", name);
				params.emplace("password", password);
				params.emplace("hwid", hwid);

				auto res = cli.Post("/verify", params);

				if (res)
				{
					server.trace_handler("status: " + std::to_string(res->status));
					server.trace_handler("body: " + res->body);
				}

				if (res && res->status == 200 && res->body.find("true") != std::string::npos)
				{
					server.trace_handler("authenticated some hoe");

					// unauthenticate dead clients, or kids logging in with same info
					while (Client* client = server.FindAuthenticatedClientByName(channel_loader, name))
					{
						if (client->id == 0)
							break;

						client->authenticated = false;

						server.trace_handler("kicked some nibba off");

						std::this_thread::sleep_for(std::chrono::milliseconds(100));
					}

					client.authenticated = true;
					client.name = name;
					client.channel = channel;

					jwt::jwt_object obj{ algorithm("ES256"), secret(ec_private_key) };

					// for now sessions will expire in 1 week, c++2a will have ::weeks
					auto time = std::chrono::system_clock::now() + std::chrono::hours(24 * 7);

					obj.add_claim("aud", name);
					obj.add_claim("exp", time);

					server.Send(client, json{ "login_response", obj.signature() });
				}
				else
				{
					server.trace_handler("someone failed to login (this is rlly bad...)");
				}
			}
			else if (channel == channel_cheat)
			{
				server.trace_handler("dump: " + message.dump());

				std::string token = message[1];
				if (token == "KyQBvHfGExV9a7Dn6wgrs8BJVkBuYN")
				{
					server.trace_handler("someone is logging in using dev token");

					client.authenticated = true;
					client.name = "nox";
					client.channel = channel_cheat;
				}
				else
				{
					server.trace_handler("someone is trying to login using session");

					try
					{
						auto dec_obj = jwt::decode(token, algorithms({ "ES256" }), secret(ec_public_key), verify(true));

						server.trace_handler(dec_obj.header().create_json_obj().dump());
						server.trace_handler(dec_obj.payload().create_json_obj().dump());

						std::string name = dec_obj.payload().create_json_obj()["aud"];

						server.trace_handler("authenticated '" + name + "' using session");

						client.authenticated = true;
						client.name = name;
						client.channel = channel;
					}
					catch (const jwt::TokenExpiredError& e)
					{
						server.trace_handler("jwt::TokenExpiredError");
					}
					catch (const jwt::SignatureFormatError& e)
					{
						server.trace_handler("jwt::SignatureFormatError");
					}
					catch (const jwt::DecodeError& e)
					{
						server.trace_handler("jwt::DecodeError");
					}
					catch (const jwt::VerificationError& e)
					{
						server.trace_handler("jwt::VerificationError");
					}
					catch (const std::exception& e)
					{
						server.trace_handler("unhandled exception in token verification");
						server.trace_handler(e.what());
					}
				}
			}
		}
		catch (const std::exception& e)
		{
			server.trace_handler("caught exception in login - channel_loader");
			server.trace_handler(e.what());
		}
	});

	server.On("set_settings", channel_cheat, [](Client& client, json& message)
	{
		try
		{
			server.trace_handler("set_settings username: " + client.name);

			httplib::Client cli("localhost", 80);

			// back we go again...
			auto cbor = json::to_cbor(message);

			httplib::Params params;
			params.emplace("username", client.name);
			params.emplace("settings", base64_encode(cbor.data(), cbor.size()));

			auto res = cli.Post("/update_settings", params);

			server.trace_handler(res->body);

			/*
			if (res->body.find("true") != std::string::npos)
			{

			}
			*/

			//for (auto& a : message)
			//{
//
			//}

			//std::string settings = message.type_name();

			server.trace_handler("settings dump: " + message.dump());
		}
		catch (const std::exception& e)
		{
			server.trace_handler("caught exception in settings - channel_cheat");
			server.trace_handler(e.what());
		}
	});

	server.On("get_settings", channel_cheat, [](Client& client, json& message)
	{
		try
		{
			server.trace_handler("habib: " + client.name);

			httplib::Client cli("localhost", 80);

			std::string url = "/get_settings?username=" + client.name;
			auto res = cli.Get(url.c_str());

			server.trace_handler(res->body);

			if (res->body == "false")
			{
				server.trace_handler("no settings set yet");

				server.Send(client, json{ "get_settings_response", { false } });
			}
			else
			{
				server.trace_handler("settings are set");

				auto settings = json::from_cbor(base64_decode(res->body));
				server.Send(client, json{ "get_settings_response", { true, settings } });
			}
		}
		catch (const std::exception& e)
		{
			server.trace_handler("caught exception in settings - get_settings");
			server.trace_handler(e.what());
		}
	});

	std::thread thread_server([&]
	{
		server.Start();
	});

	cpp_redis::active_logger = std::unique_ptr<cpp_redis::logger>(new cpp_redis::logger);
	cpp_redis::subscriber sub;

	try
	{
		sub.connect("localhost", 6379, [](const std::string& host, std::size_t port, cpp_redis::subscriber::connect_state status) {
			std::cout << "sub.connect('localhost') status: " << (int)status << "\n";

			if (status == cpp_redis::subscriber::connect_state::dropped) {
				std::cout << "client disconnected from " << host << ":" << port << std::endl;
				//should_exit.notify_all();
			}
		});

		sub.subscribe("requested_cheat", [&](const std::string& chan, const std::string& msg) {
			std::cout << "requested cheat dog\n";

			json parsed = json::parse(msg);

			int user_id = parsed[0].get<int>();
			std::string username = parsed[1].get<std::string>();
			int cheat_id = std::stoi(parsed[2].get<std::string>());

			//publish_queue.emplace_back(json(user_id, 5));

			if (cheat_id == 1)
			{
				auto dll_memory = read_file("//var//www//autism//1.dll");
				std::string base64 = base64_encode(&dll_memory[0], dll_memory.size());

				json to_send{ "csgo.exe", base64 };

				auto client = server.FindAuthenticatedClientByName(channel_loader, username);

				if (client->id != 0)
				{
					client->cheat_id = cheat_id;

					server.Send(*client, json{ "inject", to_send });

					publish_queue.emplace_back("injection_progress_update", json({ user_id, 100 }));
				}
				else
				{
					std::cout << "sum1 tryna cop" << std::endl;

					//MessageBoxA(NULL, "sum1 tryna cop", "", NULL);
				}
			}
			else if (cheat_id == 2)
			{
				auto dll_memory = read_file("//var//www//autism//2.dll");
				std::string base64 = base64_encode(&dll_memory[0], dll_memory.size());

				json to_send{ "League of Legends.exe", base64 };

				auto client = server.FindAuthenticatedClientByName(channel_loader, username);

				if (client->id != 0)
				{
					client->cheat_id = cheat_id;

					server.Send(*client, json{ "inject", to_send });

					publish_queue.emplace_back("injection_progress_update", json({ user_id, 100 }));
				}
				else
				{
					std::cout << "sum1 tryna cop" << std::endl;

					//MessageBoxA(NULL, "sum1 tryna cop", "", NULL);
				}

				//std::cout << username << cheat_id << "\n";

				//std::string_view username = json[0];

				//std::cout << username.data() << " is injecting []" << std::endl;
			}
			else if (cheat_id == 3)
			{
				auto dll_memory = read_file("//var//www//autism//3.dll");
				std::string base64 = base64_encode(&dll_memory[0], dll_memory.size());

				json to_send{ "League of Legends.exe", base64 };

				auto client = server.FindAuthenticatedClientByName(channel_loader, username);

				if (client->id != 0)
				{
					client->cheat_id = cheat_id;

					server.Send(*client, json{ "inject", to_send });

					publish_queue.emplace_back("injection_progress_update", json({ user_id, 100 }));
				}
				else
				{
					std::cout << "sum1 tryna cop" << std::endl;
				}
			}
		});

		sub.subscribe("get_initial_admin_players", [&](const std::string& chan, const std::string& msg) {
			json to_send;

			for (auto& client : server.GetConnectedClients())
			{
				if (client->channel == channel_loader)
				{
					to_send.emplace_back(json{ client->name, client->cheat_id });
				}
			}

			if (to_send.size() > 0)
			{
				publish_queue.emplace_back("initial_admin_players_update", to_send);
			}
		});

		sub.commit();
	}
	catch (std::exception& e)
	{
		std::cout << "exception in redis listener: " << e.what() << "\n";
	}

	while (1)
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//signal(SIGINT, &sigint_handler);
	//std::mutex mtx;
	//std::unique_lock<std::mutex> l(mtx);
	//should_exit.wait(l);

#ifdef _WIN32
	//WSACleanup();
#endif /* _WIN32 */

	//return 0;

	return 0;
}
