#include "network.hpp"

Network::Network()
{
	enetpp::global_state::get().initialize();

	trace_handler = [&](const std::string& msg)
	{
		std::lock_guard<std::mutex> lock(mutex);

		std::cout << msg << "\n";
	};

	client.set_trace_handler(trace_handler);
}

void Network::Connect(const std::string& hostname, enet_uint16 port)
{
	client.connect(enetpp::client_connect_params()
		.set_channel_count(1)
		.set_server_host_name_and_port(hostname.c_str(), port));
}

void Network::Disconnect()
{
	connected = false;

	client.disconnect();
}

void Network::Send(const std::string& to, const json& message)
{
	queue.emplace_back(json{ to, message });
}

void Network::Consume()
{
	static auto on_connected = [this]()
	{
		trace_handler("on_connected");

		connected = true;
	};

	static auto on_disconnected = [this]()
	{
		trace_handler("on_disconnected");
	};

	static auto on_data_received = [this](const enet_uint8* data, size_t data_size)
	{
		//MessageBoxA(NULL, "on_data_received", "on_data_received", NULL);

		try
		{
			json j_from_cbor = json::from_cbor(data, data_size);
			//MessageBoxA(NULL, "made it after from_cbor", "made it after from_cbor", NULL);

			std::string callback_name = j_from_cbor[0];
			//MessageBoxA(NULL, callback_name.c_str(), callback_name.c_str(), NULL);

			json message = j_from_cbor[1];

			for (auto& callback : callbacks)
			{
				if (callback.name == callback_name)
					callback.callback(message);
			}
		}
		catch (std::exception& e)
		{
			MessageBoxA(NULL, e.what(), e.what(), NULL);
		}
	};

	client.consume_events(on_connected, on_disconnected, on_data_received);
}

void Network::On(const std::string& name, std::function<void(json&)> function)
{
	callbacks.emplace_back(name, function);
}

void Network::Think()
{
	while (client.is_connecting_or_connected())
	{
		if (connected)
		{
			for (auto& msg : queue)
			{
				auto cbor = json::to_cbor(msg);

				client.send_packet(0, reinterpret_cast<const enet_uint8*>(cbor.data()), cbor.size(), ENET_PACKET_FLAG_RELIABLE);
			}

			queue.clear();
		}

		Consume();

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

Network::~Network()
{
	Disconnect();

	enetpp::global_state::get().deinitialize();
}
