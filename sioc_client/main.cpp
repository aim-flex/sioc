#include "network.hpp"

#include <iostream>
#include <fstream>
#include <map>
#include <chrono>
#include <string>
#include <random>
#include <conio.h>

#include "enet/enet.h"

#include "enetpp/client.h"
#include "enetpp/server.h"

#include "json.hpp"
#include "fifo_map.hpp"

#undef min
#undef max

#include "jwt/jwt.hpp"

#include <cpp_redis/cpp_redis>

Network network;

std::string read_from_file(const std::string& path)
{
	std::string contents;
	std::ifstream is{ path, std::ifstream::binary };

	if (is) {
		// get length of file:
		is.seekg(0, is.end);
		auto length = is.tellg();
		is.seekg(0, is.beg);
		contents.resize(length);

		is.read(&contents[0], length);
		if (!is) {
			is.close();
			return {};
		}
	}

	is.close();
	return contents;
}

std::string ec_public_key = "\
-----BEGIN PUBLIC KEY-----\r\n\
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEF0Gp9S8+FGQBz8w1Qzpcscev1A/y\r\n\
4cwkZ3dPzLIPoQx7TpJeCOK42FrqNpmSCjo77mii1VArK0Qoh6gye8V7pw==\r\n\
-----END PUBLIC KEY-----\r\n";

void OnExit()
{
	network.Disconnect();
}

int main()
{
	using namespace jwt::params;

	network.On("login_response", [](json& json)
	{
		network.trace_handler("login_response handler: " + json.dump());

		auto dec_obj = jwt::decode(json, algorithms({ "ES256" }), verify(false), secret(ec_public_key));
		
		network.trace_handler(dec_obj.header().create_json_obj().dump());
		network.trace_handler(dec_obj.payload().create_json_obj().dump());
	});
	
	atexit(OnExit);

	network.Connect("localhost", port);
	network.Send("login", json{ channel_loader, "nox", "CYHNm66tgHzw" });

	while (true)
	{
		network.Think();
	}

	return 0;
}
